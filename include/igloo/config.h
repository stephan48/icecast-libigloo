/* Copyright (C) 2018       Marvin Scholz <epirat07@gmail.com>
 * Copyright (C) 2018-2020  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__CONFIG_H_
#define _LIBIGLOO__CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef igloo_CONFIGURED
#define igloo_ATTR_GCC(x) __attribute__ ((x))

#ifdef igloo_INTERNAL
/* use values from <config.h> here. */

#ifdef HAVE_TYPE_ATTRIBUTE_TRANSPARENT_UNION
#define igloo_ATTR_T_TRANSPARENT_UNION igloo_ATTR_GCC(transparent_union)
#endif

#else /* ! igloo_INTERNAL */
/* guess based on other defined constants here */

#if (defined(__GNUC__) && __GNUC__ >= 4) || (defined(__clang__) && __clang_major__ >= 4)
#define igloo_ATTR_T_TRANSPARENT_UNION igloo_ATTR_GCC(transparent_union)
#endif
#endif /* ! igloo_INTERNAL */

#if defined(__clang__) && __clang_major__ >= 4
#define igloo_ATTR_F_WARN_UNUSED_RESULT igloo_ATTR_GCC(warn_unused_result)
#define igloo_ATTR_T_ALIGNED(x) igloo_ATTR_GCC(aligned (x))
#define igloo_ATTR_T_ALIGNED_EFFECTIVE
#elif defined(__GNUC__)
#if __GNUC__ >= 4
#define igloo_ATTR_F_CONST igloo_ATTR_GCC(const)
#define igloo_ATTR_F_PURE igloo_ATTR_GCC(pure)
#define igloo_ATTR_F_WARN_UNUSED_RESULT igloo_ATTR_GCC(warn_unused_result)
#define igloo_ATTR_F_MALLOC igloo_ATTR_GCC(malloc)
#define igloo_ATTR_T_ALIGNED(x) igloo_ATTR_GCC(aligned (x))
#define igloo_ATTR_T_ALIGNED_EFFECTIVE

#define igloo_HAVE_STATEMENT_EXPRESSIONS

#if __GNUC_MINOR__ >= 9
#define igloo_ATTR_F_HOT igloo_ATTR_GCC(hot)
#define igloo_ATTR_F_COLD igloo_ATTR_GCC(cold)
#define igloo_ATTR_F_ERROR(msg) igloo_ATTR_GCC(error (msg))
#define igloo_ATTR_F_WARNING(msg) igloo_ATTR_GCC(warning (msg))
#define igloo_ATTR_F_DEPRECATED(msg) igloo_ATTR_GCC(deprecated (msg))
#define igloo_ATTR_T_DEPRECATED(msg) igloo_ATTR_GCC(deprecated (msg))
#define igloo_ATTR_V_DEPRECATED(msg) igloo_ATTR_GCC(deprecated (msg))
#else
#define igloo_ATTR_F_DEPRECATED(msg) igloo_ATTR_GCC(deprecated)
#define igloo_ATTR_T_DEPRECATED(msg) igloo_ATTR_GCC(deprecated)
#define igloo_ATTR_V_DEPRECATED(msg) igloo_ATTR_GCC(deprecated)
#endif
#endif

#endif /* ! __GNUC__ */

#endif /* ! igloo_CONFIGURED */

/* Defaults for attributes */
#ifndef igloo_ATTR_F_CONST
#define igloo_ATTR_F_CONST
#endif

#ifndef igloo_ATTR_F_PURE
#define igloo_ATTR_F_PURE
#endif

#ifndef igloo_ATTR_F_HOT
#define igloo_ATTR_F_HOT
#endif

#ifndef igloo_ATTR_F_COLD
#define igloo_ATTR_F_COLD
#endif

#ifndef igloo_ATTR_F_WARN_UNUSED_RESULT
#define igloo_ATTR_F_WARN_UNUSED_RESULT
#endif

#ifndef igloo_ATTR_F_MALLOC
#define igloo_ATTR_F_MALLOC
#endif

#ifndef igloo_ATTR_F_DEPRECATED
#define igloo_ATTR_F_DEPRECATED(msg)
#endif

#ifndef igloo_ATTR_F_ERROR
#define igloo_ATTR_F_ERROR(msg)
#endif

#ifndef igloo_ATTR_F_WARNING
#define igloo_ATTR_F_WARNING(msg)
#endif

#ifndef igloo_ATTR_T_ALIGNED
#define igloo_ATTR_T_ALIGNED(x)
#endif

#ifndef igloo_ATTR_T_DEPRECATED
#define igloo_ATTR_T_DEPRECATED(msg)
#endif

#ifndef igloo_ATTR_V_DEPRECATED
#define igloo_ATTR_V_DEPRECATED(msg)
#endif

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__IGLOO_H_ */
