/*
 * The SHA3 implementation is based on rhash:
 * 2013 by Aleksey Kravchenko <rhash.admin@gmail.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <igloo/digest.h>
#include <igloo/error.h>
#include <igloo/ro.h>

#define DIGEST_ALGO_SHA3_224 "SHA3-224"
#define DIGEST_ALGO_SHA3_256 "SHA3-256"
#define DIGEST_ALGO_SHA3_384 "SHA3-384"
#define DIGEST_ALGO_SHA3_512 "SHA3-512"

struct igloo_digest_tag {
    igloo_ro_full_t __parent;

    /* metadata */
    const char *algo;

    /* state */
    int done;
    union {
        struct {
            /* 1600 bits algorithm hashing state */
            uint64_t hash[25];
            /* 1536-bit buffer for leftovers */
            char message[24*8];
            /* count of bytes in the message[] buffer */
            size_t rest;
            /* size of a message block processed at once */
            size_t block_size;
        } sha3;
    } state;
};

igloo_RO_PUBLIC_TYPE(igloo_digest_t, igloo_ro_full_t);

struct igloo_hmac_tag {
    igloo_ro_full_t __parent;

    const char *algo;
    char *key;
    size_t key_len;
    igloo_digest_t * inner;
};

static void __hmac_free(igloo_ro_t self)
{
    igloo_hmac_t *hmac = igloo_ro_to_type(self, igloo_hmac_t);
    free(hmac->key);
    igloo_ro_unref(&(hmac->inner));
}

igloo_RO_PUBLIC_TYPE(igloo_hmac_t, igloo_ro_full_t,
        igloo_RO_TYPEDECL_FREE(__hmac_free)
        );

#define ROTL64(qword, n) ((qword) << (n) ^ ((qword) >> (64 - (n))))

#ifdef WORDS_BIGENDIAN
// TODO: Improve this.
static inline uint64_t digest_letoh64(uint64_t x)
{
    union {
        uint64_t num;
        char b[8];
    } in, out;
    int i;

    in.num = x;
    for (i = 0; i < 8; i++)
        out.b[i] = in.b[7 - i];
    return out.num;
}
#else
#define digest_letoh64(x) (x)
#endif

/* SHA3 (Keccak) constants for 24 rounds */
static uint64_t keccak_round_constants[] = {
        0x0000000000000001ULL, 0x0000000000008082ULL, 0x800000000000808AULL, 0x8000000080008000ULL,
        0x000000000000808BULL, 0x0000000080000001ULL, 0x8000000080008081ULL, 0x8000000000008009ULL,
        0x000000000000008AULL, 0x0000000000000088ULL, 0x0000000080008009ULL, 0x000000008000000AULL,
        0x000000008000808BULL, 0x800000000000008BULL, 0x8000000000008089ULL, 0x8000000000008003ULL,
        0x8000000000008002ULL, 0x8000000000000080ULL, 0x000000000000800AULL, 0x800000008000000AULL,
        0x8000000080008081ULL, 0x8000000000008080ULL, 0x0000000080000001ULL, 0x8000000080008008ULL
};

static inline void sha3_init(igloo_digest_t *digest, unsigned int bits)
{
    /* The Keccak capacity parameter = bits * 2 */
    unsigned int rate = 1600 - bits * 2;

    digest->state.sha3.block_size = rate / 8;
}

#define XORED_A(i) A[(i)] ^ A[(i) + 5] ^ A[(i) + 10] ^ A[(i) + 15] ^ A[(i) + 20]
#define THETA_STEP(i) \
        A[(i)]      ^= D[(i)]; \
        A[(i) + 5]  ^= D[(i)]; \
        A[(i) + 10] ^= D[(i)]; \
        A[(i) + 15] ^= D[(i)]; \
        A[(i) + 20] ^= D[(i)]

/* Keccak theta() transformation */
static inline void keccak_theta(uint64_t *A)
{
    uint64_t D[5];
    D[0] = ROTL64(XORED_A(1), 1) ^ XORED_A(4);
    D[1] = ROTL64(XORED_A(2), 1) ^ XORED_A(0);
    D[2] = ROTL64(XORED_A(3), 1) ^ XORED_A(1);
    D[3] = ROTL64(XORED_A(4), 1) ^ XORED_A(2);
    D[4] = ROTL64(XORED_A(0), 1) ^ XORED_A(3);
    THETA_STEP(0);
    THETA_STEP(1);
    THETA_STEP(2);
    THETA_STEP(3);
    THETA_STEP(4);
}

/* Keccak pi() transformation */
static inline void keccak_pi(uint64_t *A)
{
    uint64_t A1;
    A1 = A[1];
    A[ 1] = A[ 6];
    A[ 6] = A[ 9];
    A[ 9] = A[22];
    A[22] = A[14];
    A[14] = A[20];
    A[20] = A[ 2];
    A[ 2] = A[12];
    A[12] = A[13];
    A[13] = A[19];
    A[19] = A[23];
    A[23] = A[15];
    A[15] = A[ 4];
    A[ 4] = A[24];
    A[24] = A[21];
    A[21] = A[ 8];
    A[ 8] = A[16];
    A[16] = A[ 5];
    A[ 5] = A[ 3];
    A[ 3] = A[18];
    A[18] = A[17];
    A[17] = A[11];
    A[11] = A[ 7];
    A[ 7] = A[10];
    A[10] = A1;
    /* note: A[ 0] is left as is */
}

#define CHI_STEP(i) \
        A0 = A[0 + (i)]; \
        A1 = A[1 + (i)]; \
        A[0 + (i)] ^= ~A1 & A[2 + (i)]; \
        A[1 + (i)] ^= ~A[2 + (i)] & A[3 + (i)]; \
        A[2 + (i)] ^= ~A[3 + (i)] & A[4 + (i)]; \
        A[3 + (i)] ^= ~A[4 + (i)] & A0; \
        A[4 + (i)] ^= ~A0 & A1

/* Keccak chi() transformation */
static inline void keccak_chi(uint64_t *A)
{
    uint64_t A0, A1;
    CHI_STEP(0);
    CHI_STEP(5);
    CHI_STEP(10);
    CHI_STEP(15);
    CHI_STEP(20);
}

/**
 * The core transformation. Process the specified block of data.
 *
 * @param hash the algorithm state
 * @param block the message block to process
 * @param block_size the size of the processed block in bytes
 */
static inline void sha3_process_block(uint64_t hash[25], const uint64_t *block, size_t block_size)
{
    size_t round;

    /* expanded loop */
    hash[ 0] ^= digest_letoh64(block[ 0]);
    hash[ 1] ^= digest_letoh64(block[ 1]);
    hash[ 2] ^= digest_letoh64(block[ 2]);
    hash[ 3] ^= digest_letoh64(block[ 3]);
    hash[ 4] ^= digest_letoh64(block[ 4]);
    hash[ 5] ^= digest_letoh64(block[ 5]);
    hash[ 6] ^= digest_letoh64(block[ 6]);
    hash[ 7] ^= digest_letoh64(block[ 7]);
    hash[ 8] ^= digest_letoh64(block[ 8]);
    /* if not sha3-512 */
    if (block_size > 72) {
        hash[ 9] ^= digest_letoh64(block[ 9]);
        hash[10] ^= digest_letoh64(block[10]);
        hash[11] ^= digest_letoh64(block[11]);
        hash[12] ^= digest_letoh64(block[12]);
        /* if not sha3-384 */
        if (block_size > 104) {
            hash[13] ^= digest_letoh64(block[13]);
            hash[14] ^= digest_letoh64(block[14]);
            hash[15] ^= digest_letoh64(block[15]);
            hash[16] ^= digest_letoh64(block[16]);
            /* if not sha3-256 */
            if (block_size > 136) {
                hash[17] ^= digest_letoh64(block[17]);
            }
        }
    }

    /* make a permutation of the hash */
    for (round = 0; round < (sizeof(keccak_round_constants)/sizeof(*keccak_round_constants)); round++) {
        keccak_theta(hash);

        /* apply Keccak rho() transformation */
        hash[ 1] = ROTL64(hash[ 1],  1);
        hash[ 2] = ROTL64(hash[ 2], 62);
        hash[ 3] = ROTL64(hash[ 3], 28);
        hash[ 4] = ROTL64(hash[ 4], 27);
        hash[ 5] = ROTL64(hash[ 5], 36);
        hash[ 6] = ROTL64(hash[ 6], 44);
        hash[ 7] = ROTL64(hash[ 7],  6);
        hash[ 8] = ROTL64(hash[ 8], 55);
        hash[ 9] = ROTL64(hash[ 9], 20);
        hash[10] = ROTL64(hash[10],  3);
        hash[11] = ROTL64(hash[11], 10);
        hash[12] = ROTL64(hash[12], 43);
        hash[13] = ROTL64(hash[13], 25);
        hash[14] = ROTL64(hash[14], 39);
        hash[15] = ROTL64(hash[15], 41);
        hash[16] = ROTL64(hash[16], 45);
        hash[17] = ROTL64(hash[17], 15);
        hash[18] = ROTL64(hash[18], 21);
        hash[19] = ROTL64(hash[19],  8);
        hash[20] = ROTL64(hash[20], 18);
        hash[21] = ROTL64(hash[21],  2);
        hash[22] = ROTL64(hash[22], 61);
        hash[23] = ROTL64(hash[23], 56);
        hash[24] = ROTL64(hash[24], 14);

        keccak_pi(hash);
        keccak_chi(hash);

        /* apply iota(hash, round) */
        *hash ^= keccak_round_constants[round];
    }
}

static void sha3_write(igloo_digest_t *digest, const char *msg, size_t size)
{
    size_t rest = digest->state.sha3.rest;
    size_t block_size = digest->state.sha3.block_size;

    digest->state.sha3.rest = (rest + size) % block_size;

    /* fill partial block */
    if (rest) {
        size_t left = block_size - rest;
        memcpy(digest->state.sha3.message + rest, msg, (size < left ? size : left));
        if (size < left) return;

        /* process partial block */
        sha3_process_block(digest->state.sha3.hash, (uint64_t*)digest->state.sha3.message, block_size);
        msg  += left;
        size -= left;
    }
    while (size >= block_size) {
        const char *aligned_message_block;

        if (((intptr_t)(void*)msg) & 7) {
            memcpy(digest->state.sha3.message, msg, block_size);
            aligned_message_block = digest->state.sha3.message;
        } else {
            aligned_message_block = msg;
        }

        sha3_process_block(digest->state.sha3.hash, (uint64_t*)aligned_message_block, block_size);
        msg  += block_size;
        size -= block_size;
    }

    if (size)
        memcpy(digest->state.sha3.message, msg, size); /* save leftovers */
}

static inline size_t sha3_read(igloo_digest_t *digest, void *buf, size_t len)
{
    const size_t block_size = digest->state.sha3.block_size;

    memset(digest->state.sha3.message + digest->state.sha3.rest, 0, block_size - digest->state.sha3.rest);
    digest->state.sha3.message[digest->state.sha3.rest] |= 0x06;
    digest->state.sha3.message[block_size - 1] |= 0x80;

    sha3_process_block(digest->state.sha3.hash, (uint64_t*)digest->state.sha3.message, block_size);

#ifdef WORDS_BIGENDIAN
    do {
        size_t i;
        for (i = 0; i < (sizeof(digest->state.sha3.hash)/sizeof(*digest->state.sha3.hash)); i++)
            digest->state.sha3.hash[i] = digest_htole64(digest->state.sha3.hash[i]);
    } while (0);
#endif

    if (len > (100 - digest->state.sha3.block_size / 2))
        len = 100 - digest->state.sha3.block_size / 2;
    memcpy(buf, digest->state.sha3.hash, len);
    return len;
}

igloo_error_t igloo_digest_new(igloo_digest_t **self, igloo_ro_t group, const char *algo)
{
    igloo_error_t err;

    err = igloo_ro_new_raw(self, igloo_digest_t, group);
    if (err != igloo_ERROR_NONE) {
        return err;
    }

    /* strcmp is not defined for NULL */
    if (!algo) {
        igloo_ro_unref(self);
        return igloo_ERROR_FAULT;
    }

    if (strcmp(algo, DIGEST_ALGO_SHA3_224) == 0) {
        sha3_init(*self, 224);
    } else if (strcmp(algo, DIGEST_ALGO_SHA3_256) == 0) {
        sha3_init(*self, 256);
    } else if (strcmp(algo, DIGEST_ALGO_SHA3_384) == 0) {
        sha3_init(*self, 384);
    } else if (strcmp(algo, DIGEST_ALGO_SHA3_512) == 0) {
        sha3_init(*self, 512);
    } else {
        igloo_ro_unref(self);
        return igloo_ERROR_NOSYS;
    }

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_digest_copy(igloo_digest_t **new, igloo_ro_t group, igloo_digest_t *old)
{
    igloo_error_t err;

    if (!old)
        return igloo_ERROR_FAULT;

    err = igloo_ro_new_raw(new, igloo_digest_t, group);
    if (err != igloo_ERROR_NONE) {
        return err;
    }

    (*new)->algo = old->algo;
    (*new)->done = old->done;
    (*new)->state = old->state;

    return igloo_ERROR_NONE;
}

ssize_t igloo_digest_write(igloo_digest_t *digest, const void *data, size_t len)
{
    if (!digest || digest->done || !data)
        return -1;

    sha3_write(digest, data, len);
    return len;
}

ssize_t igloo_digest_read(igloo_digest_t *digest, void *buf, size_t len)
{
    if (!digest || digest->done || !buf)
        return -1;

    digest->done = 1;

    return sha3_read(digest, buf, len);
}

static ssize_t __digest_algo_blocklength(const char *algo)
{
    /* strcmp is not defined for NULL */
    if (!algo) {
        return -1;
    }

    // 1600bit - 2 * digestlen[bit]:
    if (strcmp(algo, DIGEST_ALGO_SHA3_224) == 0) return 144;
    if (strcmp(algo, DIGEST_ALGO_SHA3_256) == 0) return 136;
    if (strcmp(algo, DIGEST_ALGO_SHA3_384) == 0) return 104;
    if (strcmp(algo, DIGEST_ALGO_SHA3_512) == 0) return 72;

    return -1; // invalid algorithm
}

static ssize_t __digest_algo_length_bytes(const char *algo)
{
    /* strcmp is not defined for NULL */
    if (!algo) {
        return -1;
    }

    if      (strcmp(algo, DIGEST_ALGO_SHA3_224) == 0) { return 224/8; }
    else if (strcmp(algo, DIGEST_ALGO_SHA3_256) == 0) { return 256/8; }
    else if (strcmp(algo, DIGEST_ALGO_SHA3_384) == 0) { return 384/8; }
    else if (strcmp(algo, DIGEST_ALGO_SHA3_512) == 0) { return 512/8; }
    else                                              { return -1;    }
}

igloo_error_t igloo_hmac_new(igloo_hmac_t **self, igloo_ro_t group, const char *algo, const void *key, size_t keylen)
{
    ssize_t blocklen;
    char *keycopy;
    igloo_error_t err;
    size_t i;
    igloo_digest_t *inner;

    if (!self || igloo_ro_is_null(group) || !algo || !key)
        return igloo_ERROR_FAULT;

    blocklen = __digest_algo_blocklength(algo);
    if (blocklen < 1) {
        return igloo_ERROR_NOSYS;
    }

    keycopy = calloc(1, blocklen);
    if (!keycopy)
        return igloo_ERROR_NOMEM;

    if (keylen > (size_t)blocklen) {
        ssize_t digestlen = __digest_algo_length_bytes(algo);
        igloo_digest_t *digest;

        if (digestlen < 1) {
            free(keycopy);
            return igloo_ERROR_NOSYS;
        }

        if ((err = igloo_digest_new(&digest, group, algo)) != igloo_ERROR_NONE) {
            free(keycopy);
            return err;
        }

        if (igloo_digest_write(digest, key, keylen) != (ssize_t)keylen) {
            igloo_ro_unref(&digest);
            free(keycopy);
            return igloo_ERROR_GENERIC;
        }

        if (igloo_digest_read(digest, keycopy, digestlen) != (ssize_t)digestlen) {
            igloo_ro_unref(&digest);
            free(keycopy);
            return igloo_ERROR_GENERIC;
        }

        igloo_ro_unref(&digest);
    } else {
        memcpy(keycopy, key, keylen);
    }

    err = igloo_ro_new_raw(self, igloo_hmac_t, group);
    if (err != igloo_ERROR_NONE) {
        return err;
    }

    (*self)->algo = algo;
    (*self)->key = keycopy;
    (*self)->key_len = blocklen;

    if ((err = igloo_digest_new(&inner, group, algo)) != igloo_ERROR_NONE)
        return err;

    (*self)->inner = inner;

    for (i = 0; i < (size_t)blocklen; i++) {
        char x = keycopy[i] ^ 0x36;

        if (igloo_digest_write((*self)->inner, &x, 1) != 1) {
            igloo_ro_unref(self);
            return igloo_ERROR_GENERIC;
        }
    }

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_hmac_copy(igloo_hmac_t **new, igloo_ro_t group, igloo_hmac_t *old)
{
    igloo_error_t err;
    igloo_digest_t *inner;

    if (!old)
        return igloo_ERROR_FAULT;

    err = igloo_ro_new_raw(new, igloo_hmac_t, group);
    if (err != igloo_ERROR_NONE)
        return err;

    (*new)->algo = old->algo;
    (*new)->key = malloc(old->key_len);
    if (!(*new)->key) {
        igloo_ro_unref(new);
        return igloo_ERROR_NOMEM;
    }
    memcpy((*new)->key, old->key, old->key_len);
    (*new)->key_len = old->key_len;

    err = igloo_digest_copy(&inner, group, old->inner);
    if (err != igloo_ERROR_NONE) {
        igloo_ro_unref(new);
        return err;
    }
    (*new)->inner = inner;

    return igloo_ERROR_NONE;
}

ssize_t igloo_hmac_write(igloo_hmac_t *hmac, const void *data, size_t len)
{
    if (!hmac)
        return -1;

    return igloo_digest_write(hmac->inner, data, len);
}

ssize_t igloo_hmac_read(igloo_hmac_t *hmac, void *buf, size_t len)
{
    igloo_digest_t *digest;
    igloo_error_t err;
    ssize_t digestlen;
    char *res;
    size_t i;
    ssize_t ret;

    if (!hmac)
        return -1;

    err = igloo_digest_new(&digest, hmac, hmac->algo);
    if (err != igloo_ERROR_NONE)
        return -1;

    digestlen = __digest_algo_length_bytes(hmac->algo);
    if (digestlen < 1)
        return -1;

    res = malloc(digestlen);
    if (!res) {
        igloo_ro_unref(&digest);
        return -1;
    }

    for (i = 0; i < hmac->key_len; i++) {
        char x = hmac->key[i] ^ 0x5C;
        if (igloo_digest_write(digest, &x, 1) != 1) {
            free(res);
            igloo_ro_unref(&digest);
            return -1;
        }
    }

    if (igloo_digest_read(hmac->inner, res, digestlen) != (ssize_t)digestlen) {
        free(res);
        igloo_ro_unref(&digest);
        return -1;
    }

    if (igloo_digest_write(digest, res, digestlen) != (ssize_t)digestlen) {
        free(res);
        igloo_ro_unref(&digest);
        return -1;
    }

    ret = igloo_digest_read(digest, buf, len);

    free(res);
    igloo_ro_unref(&digest);

    return ret;
}
